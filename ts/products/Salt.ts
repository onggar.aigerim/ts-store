import Product from "./Product";
import { StoragePlace } from "./StoragePlace";

class Salt extends Product {
  protected name: string;
  protected storageLifeDays: number;
  protected storagePlace: StoragePlace;
  protected deliveryTimestamp: Date;

  constructor(storagePlace: StoragePlace, deliveryTimestamp: Date) {
    super();
    this.name = "Соль";
    this.storagePlace = storagePlace;
    this.deliveryTimestamp = deliveryTimestamp;
    this.storageLifeDays = Number.POSITIVE_INFINITY;
  }

  isFresh(): boolean {
      return true;
  }
}
export default Salt;