import {StoragePlace} from './StoragePlace';
import {getSringRow} from "../helpers/helpFunctions"

abstract class Product {
  protected abstract name: string;
  protected abstract storageLifeDays: number;
  protected abstract storagePlace: StoragePlace;
  protected abstract deliveryTimestamp: Date;
  abstract isFresh(): boolean;

  toSting(): string {
    const deliveryTimestamp: string = this.deliveryTimestamp.toLocaleDateString();
    const storageLifeDays: string = this.storageLifeDays.toString();
    const fresh: string = this.isFresh().toString();
    return getSringRow(this.name, deliveryTimestamp, this.storagePlace, storageLifeDays, fresh);
  }
}

export default Product;