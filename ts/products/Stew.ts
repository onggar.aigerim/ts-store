import Product from "./Product";
import { StoragePlace } from "./StoragePlace";

class Stew extends Product {
  protected name: string;
  protected storageLifeDays: number;
  protected storagePlace: StoragePlace;
  protected deliveryTimestamp: Date;

  constructor(storagePlace: StoragePlace, deliveryTimestamp: Date) {
    super();
    this.name = "Тушёнка";
    this.storagePlace = storagePlace;
    this.deliveryTimestamp = deliveryTimestamp;
    this.storageLifeDays = 180;
  }

  isFresh(): boolean {
    let freshDay = this.storageLifeDays * 86400000;
    let deliveryNumber: number = Date.parse(this.deliveryTimestamp.toISOString());
    let generalFreshDayNumber: number = freshDay + deliveryNumber;
    if (generalFreshDayNumber > Date.now()) {
      return true;
    } else {
      return false;
    }
  }
}

export default Stew;