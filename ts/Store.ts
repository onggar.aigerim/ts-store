import Product from "./products/Product";
import { GetRandomNumber } from "./helpers/helpFunctions";
import { getSringRow } from "./helpers/helpFunctions";
import { StoragePlace } from "./products/StoragePlace";
import Salt from "./products/Salt";
import Milk from "./products/Milk";
import Fish from "./products/Fish";
import Corn from "./products/Corn";
import Stew from "./products/Stew";

class Store {
  private _products: Array<Product> = [];

  constructor() {
    for (let i = 0; i < 20; i++) {
      const product: Product = this.getRandomProduct();
      this._products.push(product);
    }
  }

  private getRandomDeliveryTime(): Date {
    const randomNumber = GetRandomNumber(1, 200);
    let deliveryTimestamp: Date = new Date(Date.now() - 86400000 * randomNumber)
    return deliveryTimestamp;
  }

  private GetRandomStoragePlace(): StoragePlace {
    const randomPlace = GetRandomNumber(1, 100)
    if (randomPlace < 50) {
      return StoragePlace.Icebox;
    } else {
      return StoragePlace.Showcase
    }
  }
  
  getRandomProduct(): Product {
    const products: Product[] = [
      new Salt(this.GetRandomStoragePlace(), this.getRandomDeliveryTime()),
      new Milk(this.GetRandomStoragePlace(), this.getRandomDeliveryTime()),
      new Fish(this.GetRandomStoragePlace(), this.getRandomDeliveryTime()),
      new Corn(this.GetRandomStoragePlace(), this.getRandomDeliveryTime()),
      new Stew(this.GetRandomStoragePlace(), this.getRandomDeliveryTime()),
    ]
    const randomIndex = GetRandomNumber(0, products.length - 1)
    return products[randomIndex]
  }

  doInspection(): void {
    let message = console.log("Результаты инспекции".padStart(50) + "\n" + getSringRow("Название продукта", "Дата привоза", "Место хранение", "Срок хранение", "Свежесть") + "\n---------------------+-----------------+-----------------+-----------------+----------");
    for (let i = 0; i < this._products.length; i++) {
      console.log(this._products[i].toSting())
    }
    console.log("---------------------+-----------------+-----------------+-----------------+----------")
  }
}

export default Store;