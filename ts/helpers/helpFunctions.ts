export const GetRandomNumber = (min: number, max: number): number =>  Math.floor(Math.random() * (max - min + 1));
export const getSringRow = (name: string, deliveryTimestamp: string, storagePlace: string, storageLifeDays: string, fresh: string): string => {
  return `${name.padEnd(20)} | ${deliveryTimestamp.padEnd(15)} | ${storagePlace.padEnd(15)} | ${storageLifeDays.padEnd(15)} | ${fresh.padEnd(10)}`
}